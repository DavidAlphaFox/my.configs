# My i3config!
#
# Please see https://www.gitlab.com/ercdude/my.configs for details

### Setup ###
# Set main key as "winkey"
set $mod mod4

# Apps
set $term urxvt
set $fterm urxvt -name floating -depth 32 -bg rgba:3579/1212/1212/dada
set $top urxvt -name floating -depth 32 -bg rgba:7979/0303/0000/5555 -e gotop -s -c monokai
set $agenda urxvt -name floating -depth 32 -bg rgba:4848/0000/0000/5555 -e calcurse
set $explorer urxvt -name floating -depth 32 -bg rgba:fafa/fafa/fafa/8888 -fg rgba:0000/0000/0000/ffff -e ranger
set $ide emacs
set $virtualbox VirtualBox
set $calc xcalc
set $menu dmenu_run
set $browser firefox
set $bg feh --bg-center ~/Images/.bg
set $printscreen my.printscreen

# Actions/scripts
set $lock my.lock
set $bar_restart my.bar restart
set $x_restart my.x restart
set $vol_down mixer vol -5 pcm -5
set $vol_up mixer vol +5 pcm +5

# Compton scripts
set $set_opaque ts_opaque_active
set $set_transparency ts_set_transparency
set $blink_active ts_blink_active
set $toggle_opacity ts_toggle_active
set $inc_opacity ts_inc_active
set $dec_opacity ts_dec_active

# Remove stealing foccus whem mouse change workspace
mouse_warping none

# Set up bg
exec $bg
exec setxkbmap br
exec $bar_restart
exec_always compton --inactive-dim-fixed -f -b --config ~/.config/compton/compton.conf

# Setup Windows
#floating_minimum_size 30x50
floating_modifier mod1
title_align center
focus_follows_mouse no

# Setup Workspace
workspace_auto_back_and_forth yes
workspace_layout tabbed
workspace "2" output eDP-1
workspace "1" output HDMI1

# Setup apps
for_window [class="XCalc"] move scratchpad, scratchpad show
for_window [class="URxvt" instance="floating"] floating enable

###############################################################################

### Visual ###
#font terminus-font 8

# Border
default_border pixel 1
hide_edge_borders smart

# colors from resource
set_from_resource $fg-color   *.foreground #aaaaaa
set_from_resource $bg-color   *.background #000000
set_from_resource $black      *.color0     #202020
set_from_resource $red        *.color1     #981515
set_from_resource $green      *.color2     #529011
set_from_resource $yellow     *.color3     #eab93d
set_from_resource $blue       *.color4     #204a87
set_from_resource $magenta    *.color5     #ce5c00
set_from_resource $cyan       *.color6     #89b6e2
set_from_resource $white      *.color7     #565567
# Light colors
set_from_resource $lblack     *.color8     #606060
set_from_resource $lred       *.color9     #ff8d8d
set_from_resource $lgreen     *.color10    #c8e7a8
set_from_resource $lyellow    *.color11    #ffc123
set_from_resource $lblue      *.color12    #3465a4
set_from_resource $lmagenta   *.color13    #f57900
set_from_resource $lcyan      *.color14    #46a4ff
set_from_resource $lwhite     *.color15    #ffffff

set_from_resource $l_black    *.foreground #aaaaaa

# class                 border    backgr.     text      indicator child_border
client.focused          $lblack   $bg-color   $lwhite   $lred     $red
client.focused_inactive $black    $bg-color   $white    #484e50   $lblack
client.unfocused        #222222   #222222     #888888   #292d2e   #000000
client.urgent           $lred     #900000     #ffffff   #900000   #900000
client.placeholder      #000000   #000000     #ffffff   #000000   #000000

client.background       #ffffff

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
# font pango:Roboto Sans Mono 8

# Before i3 v4.8, we used to recommend this one as the default:
# font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, its unicode glyph coverage is limited, the old
# X core fonts rendering does not support right-to-left and this being a bitmap
# font, it doesn’t scale on retina/hidpi displays.

###############################################################################

### Actions ###
# Restart i3 - deprecated. See restart_mode
#bindsym $mod+Shift+R restart
#bindsym $mod+Shift+B exec my.bar restart

# Close i3
bindsym $mod+Shift+Escape exit

# Lock i3
bindsym $mod+Shift+x exec $lock

# Start a terminal
bindsym $mod+Return exec $term
bindsym $mod+Shift+Return exec $fterm

# Split
bindsym $mod+ctrl+h split h
bindsym $mod+ctrl+v split v

# Toggle border
bindsym $mod+b border toggle

### Workspace ###
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

### Program Launcher ###
# start dmenu (a program launcher)
bindsym $mod+d exec $menu

### Multimedia ###
bindsym $mod+equal exec $vol_up
bindsym $mod+minus exec $vol_down

###############################################################################

### Windows ###
# Change focus
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right
bindsym $mod+h focus left
bindsym $mod+k focus up
bindsym $mod+j focus down
bindsym $mod+l focus right

# Move
bindsym $mod+shift+Left move left
bindsym $mod+shift+Down move down
bindsym $mod+shift+Up move up
bindsym $mod+shift+Right move right
bindsym $mod+shift+h move left
bindsym $mod+shift+k move up
bindsym $mod+shift+j move down
bindsym $mod+shift+l move right

# Fullscreen
bindsym $mod+f fullscreen toggle
# Stacked
bindsym $mod+w layout toggle

# Scratchpad
bindsym $mod+control+s move scratchpad
bindsym $mod+s scratchpad show

# Sticky window - it removes window from scratchpad
bindsym $mod+i sticky toggle

# Floating
bindsym $mod+y floating toggle

# kill window
bindsym $mod+Shift+q kill

set $compton_mode Compton Mode | (f)lash window | Opacity: - (s)et on - (r)emove - (t)oggle - (i)ncrease - (d)ecrease | (Q)uit mode
bindsym $mod+Shift+c mode "$compton_mode"

set $window_mode Window Edit | (f)ullscreen - (y) toggle floating - (s)cratchpad - split: (v) or (h) - (k)ill | (Q)uit mode
bindsym $mod+Shift+w mode "$window_mode"

set $apps_mode Apps | (a)genda - (w)ww - emac(s) - (e)xplorer - (t)op - (c)alc - (v)irtualbox | (Return) menu | (Q)uit mode
bindsym $mod+Shift+a mode "$apps_mode"

set $scratch_mode Scratchpad | (g)otop - (a)genda - (t)elegram - (c)alc - (v)irtualbox | (Q)uit mode
bindsym $mod+Shift+s mode "$scratch_mode"

set $restart_mode Restart | choose: poly(b)ar - (r)eload x - restart (x), no reloading files | (Q)uit mode
bindsym $mod+Shift+r mode "$restart_mode"

mode "$compton_mode" {
    # Compton
    # flash window
    bindsym f exec $blink_active, mode "default"
    bindsym t exec $toggle_opacity, mode "default"
    bindsym r exec $set_opaque, mode "default"
    bindsym s exec $set_transparency, mode "default"

    bindsym i exec $inc_opacity
    bindsym d exec $dec_opacity

    bindsym q mode "default"
    bindsym Escape mode "default"
    bindsym Return mode "default"
}

# Bind Window modifier
mode "$window_mode" {
    bindsym k kill, mode "default"
    bindsym f fullscreen toggle, mode "default"
    bindsym y floating toggle, mode "default"
    bindsym h split h, mode "default"
    bindsym v split v, mode "default"
    bindsym s move scratchpad, mode "default"
    bindsym p exec $printscreen

    # Resize
    bindsym Mod1+Up resize shrink up 10
    bindsym Mod1+Down resize shrink down 10
    bindsym Mod1+Left resize shrink left 10
    bindsym Mod1+Right resize shrink right 10
    bindsym control+Up resize grow up 10
    bindsym control+Down resize grow down 10
    bindsym control+Left resize grow left 10
    bindsym control+Right resize grow right 10

    # Move
    bindsym Up move up 20
    bindsym Down move down 20
    bindsym Left move left 20
    bindsym Right move right 20

    bindsym q mode "default"
    bindsym Escape mode "default"
    bindsym Return mode "default"
}

# Bind Application modifier
mode "$apps_mode" {
    bindsym t exec $top, mode "default"
    bindsym a exec $agenda, mode "default"
    bindsym e exec $explorer, mode "default"
    bindsym c exec $calc, mode "default"
    bindsym v exec $virtualbox, mode "default"
    bindsym w exec $browser, mode "default"
    bindsym s exec $ide, mode "default"

    bindsym q mode "default"
    bindsym Escape mode "default"
    bindsym Return exec $menu, mode "default"
}

# Bind Scratch modifier
mode "$scratch_mode" {
    bindsym g [title="gotop"] scratchpad show, mode "default"
    bindsym a [title="calcurse"] scratchpad show, mode "default"
    bindsym c [class="XCalc"] scratchpad show, mode "default"
    bindsym v [title="VirtualBox"] scratchpad show, mode "default"
    bindsym t [title="Telegram*"] scratchpad show, mode "default"

    bindsym q mode "default"
    bindsym Escape mode "default"
    bindsym Return mode "default"
}

# Restart modifier
mode "$restart_mode" {
    bindsym b exec $bar_restart, mode "default"
    bindsym r reload, mode "default"
    # It's not possible to restart i3 and polybar at the same time directly from here.
    # once restart is called, all stops. So a script is needed.
    bindsym x exec $x_restart

    bindsym q mode "default"
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
